FROM voidlinux/voidlinux
MAINTAINER cvadmins version: 1.0.0

RUN xbps-install -Syu && xbps-install -Sy python
RUN ln -s /etc/sv/sshd /etc/runit/runsvdir/current/
RUN echo 'root:cv' | chpasswd -c SHA512

EXPOSE 22

CMD ["/usr/sbin/init"]
